FROM bitnami/postgresql:14.7.0-debian-11-r18
LABEL author="SOCOM"
LABEL description="Testing Anchore scans using compliance framework"
LABEL version="1.0"
ENV POSTGRES_DB="test"
ENV POSTGRES_USER="admin"
ENV POSTGRES_PASSWORD="admin"
